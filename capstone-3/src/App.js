import {useState, useEffect} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar.js';
import {Container} from 'react-bootstrap/';
import{BrowserRouter as Router, Route, Routes} from 'react-router-dom/';
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import UpdateProduct from './pages/UpdateProduct.js';
import NewProduct from './pages/NewProduct.js';
import ProductsView from './components/ProductsView.js';
import Register from './pages/Register.js';
import CustomerPage from './pages/CustomerPage.js';
import AdminPage from './pages/AdminPage.js'
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import { UserProvider } from './UserContext';


function App() {

  const [user, setUser] = useState({

    id: null,
    isAdmin: null
    
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })

  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'POST',
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

     
      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }
      
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])


  return (
   
  <UserProvider value={{user, setUser, unsetUser}}>
   <> 
    <Router>
      <AppNavbar/>

      <Container id="container">
        <Routes>
            <Route exact path="/" element={<Home/>}/>
           
            
            <Route exact path="/productsView/:productId" element={<ProductsView/>}/>
            <Route exact path ="/admin" element={<AdminPage/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/products" element={<Products/>}/>
            <Route exact path ="/user/:userId" element={<CustomerPage/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="/addProduct" element={<NewProduct/>}/>
            <Route exact path ="/:itemId/update" element={<UpdateProduct/>}/>
            <Route path='*' element={<NotFound/>}/>

        </Routes>
      </Container>        
    </Router>
  </>
  </UserProvider>
  );

}

export default App;
