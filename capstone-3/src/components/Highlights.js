import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Highlights(){
	return(
		<Row className="m-3">
            <Col xs={12} md={4}>
                <Card id="card1" className="cardHighlight p-3">
                    <Card.Body>
                    
                        <Card.Title>
                            <h2>Pikachu</h2>
                        </Card.Title>
                        <Card.Text>
                            Electric-type mouse Pokémon
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/products">View Products</Link>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card id="card2" className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Charmander</h2>
                        </Card.Title>
                        <Card.Text>
                            Fire-type lizard Pokémon
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/products">View Products</Link>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card id="card3" className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Bulbasaur</h2>
                        </Card.Title>
                        <Card.Text>
                            Grass-type seed Pokémon
                        </Card.Text>
                        <Link className= 'btn btn-primary' to ="/notfound" disabled>Coming Soon!</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>


		)
}