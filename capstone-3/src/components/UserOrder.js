import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function CustDash({orderProps}){
	const{purchasedOn, _id, products, totalAmount}= orderProps;

	return(
		<Card className ='mx-4 my-3 p-3 text-justify'>
			<Card.Body>
				<Card.Title>Transaction ID:</Card.Title>
				<Card.Text>{_id}</Card.Text>
				<Card.Subtitle>Product ID:</Card.Subtitle>
				<Card.Text>{products.productId}</Card.Text>
				<Card.Subtitle >Quantity:</Card.Subtitle>
				<Card.Text>{products.quantity} </Card.Text>
				<Card.Subtitle >Total:</Card.Subtitle>
				<Card.Text>{totalAmount}</Card.Text>
				<Card.Subtitle >Transaction Date:</Card.Subtitle>
				<Card.Text>Date: {purchasedOn}</Card.Text>
			</Card.Body>
		</Card>

		)
}
