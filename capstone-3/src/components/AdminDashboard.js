import {Card, Button, Form} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function Dashboard({productProps}){

	const{name,description,price,_id,isActive}= productProps;

	const activate = (e) => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productProps._id}/avail`,{
			method:"PUT",
			headers:{
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		})
	}

	const archive = (e) => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productProps._id}/archive`,{
			method:"PUT",
			headers:{
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		})
	}

	return(
		(productProps.isActive)?
		<Form onSubmit ={(e) => archive(e)} >
		<Card className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Link className= 'btn btn-primary ml-2' to ={`/${_id}/update`}>Update</Link>
	 			<Button variant="danger" type="submit" controllId = "submitBtn" className= "mx-2">
					Close
				</Button>
			</Card.Body>
		</Card>
		</Form>
		:
		<Form onSubmit ={(e) => activate(e)}>
		<Card  className ='mx-4 my-3 p-3'>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Link className= 'btn btn-primary ml-2' to ={`/update/${_id}`}>Update</Link>
	 			<Button variant="success" type="submit" controllId = "submitBtn" className= "mx-2">
					Open
				</Button>
			</Card.Body>
		</Card>
		</Form>

		)
}
