import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useState} from 'react';
import {Link} from 'react-router-dom';

export default function ProductsCard({productProps}){
	console.log(productProps);
	console.log(typeof productProps);

	const {name, description, price, _id} = productProps;

	const[count, setCount] = useState(0);
	const[stock, setStock] = useState();
	console.log(useState(0));
	



	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 4, offset: 4 }}>
					<Card className="m-4">
	        <Card.Body className="text-center">
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Item Code:</Card.Subtitle>
	            <Card.Text>{_id}</Card.Text>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Link className="btn btn-primary" to={`/ProductsView/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>

				</Col>
			</Row>
		</Container>
	
		
		)
}


