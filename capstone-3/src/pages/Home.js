import Banner from '../components/Banner.js';
import Highligths from '../components/Highlights.js';

const data = {
	title: "Pokémon",
	content: "Gotta Catch ’Em All!",
	destination: "/products",
	label: "View List"
}

export default function Home() {
	return(
		<>
			<Banner data={data}/>
	      	<Highligths/>
      	</>
		)
}




