import {Card, Button, Form } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate, Link, useNavigate, useParams} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function UpdateProduct(){
	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	const [ name, setProductName ] = useState('');
	const [ description, setProductDesc ] = useState('');
	const [ price, setProductPrice ] = useState(0);

	const { productId } = useParams();

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {
		console.log(productId);

		if(name !=='' || description !=='' || price !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	})

	const update =(e) => {
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/`, {
			method:"PUT",
			headers:{
				"Content-Type" :"application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description:description,
				price:price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
			Swal.fire({
				title:"Updated!",
				icon:"success",
				text:`You have successfully updated this product!`
			})
			setProductName('');
			setProductDesc('');
			setProductPrice('');
			navigate("/admin");
		} else {
			Swal.fire({
				title: "Error Occured!",
				icon:"error",
				text:"Please try again!"
				})
			}
		})

	}

	return(
	
		<Form onSubmit ={(e) =>update(e)}>
			<Form.Group classNem='my-1' controllId='itemName'>
				<Form.Label>New Item Name:</Form.Label>
				<Form.Control
				type ='text'
				placeholder ='New Product Name'
				value={name}
				onChange ={e => setProductName(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group classNem='my-1' controllId='itemDesc'>
				<Form.Label>New Item Description:</Form.Label>
				<Form.Control
				type ='text'
				placeholder ='New Product Description'
				value={description}
				onChange ={e => setProductDesc(e.target.value)}
				required/>
			</Form.Group>

			<Form.Group classNem='mb-1' controllId='itemPrice'>
				<Form.Label>New Item Price:</Form.Label>
				<Form.Control
				type ='number'
				placeholder ='New Product Price'
				value={price}
				onChange ={e => setProductPrice(e.target.value)}
				required/>
			</Form.Group>
			{
				(isActive) ?
				<Button variant="primary" type="submit" controllId = "submitBtn">
				Update
				</Button>
				:
				<Button variant="primary" type="submit" controlId="submitBtn" disabled>
				Update
				</Button>
			}
			<Link className= 'btn btn-danger m-3'to='/admin'> Back </Link>
			
		</Form>
		)
}
