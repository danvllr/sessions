// [SECTION] - if, else if, else statement

// if

let numA = -1 

/*
Syntax:

if(condition){
	//code
}
*/

// true condition
if(numA < 0) {
	console.log("Hello");
}


// false condition -> will not execute the code
if(numA < -5) {
	console.log("Hello");
}

// String
let city = "New York";

if(city === "New York") {
	console.log("Welconme to New York City!");
}

// else if

let numH = 100;

if(numH < 0) {
	console.log("Hello!");
}else if(numH > 0) {
	console.log("World!");
}

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City!")
}else if(city === "Tokyo") {
	console.log("Welcome to Tokyo Japan!")
}

// else statement

let num1 = 5;

if(num1 === 0) {
	console.log("Hello");
}else if(num1 === 3) {
	console.log("World");
}else {
	console.log("Sorry, all conditions are not met.")
}

// if, if else, else with function

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return "Not a typhoon yet.";
	}else if(windSpeed <= 61) {
		return "Tropical Depression detected.";
	}else if (windSpeed >= 62 && windSpeed <= 88) {
		return "Tropical Storm detected.";
	}else if(windSpeed >= 89 && windSpeed <= 117) {
		return "Severe trophical storm detected.";
	}else {
		return "Typhoon detected.";
	}
}

let message = determineTyphoonIntensity(25);
message = determineTyphoonIntensity(85);
message = determineTyphoonIntensity(250);
console.log(message);

// Truthy

if(true) {
	console.log("truthy");
}

if(1) {
	console.log("truthy");
}

if([]) {
	console.log("truthy");
}

// Falsy

if(false) {
	console.log("falsy");
}

if(0) {
	console.log("falsy");
}

if(undefined) {
	console.log("falsy");
}

// Single statement execution
// Ternary Statement ?-if :-else

let num2 = 10

let ternary = (num2 > 18 || num2 === "10") ? "Yes" : "No";
console.log(ternary);

// Multiple Statement

let name;

function isOfLegalAge() {
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "You are under the age limit"
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in fuctions: " + legalAge + ", " + name);

// switch - case statement

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case "monday" :
		console.log("The color of the day is red.");
		break;
	case "tuesday" :
		console.log("The color of the day is orange.");
		break;
	case "wednesday" :
		console.log("The color of the day is yellow.");
		break;
	case "thurday" :
		console.log("The color of the day is green.");
		break;
	case "friday" :
		console.log("The color of the day is blue.");
		break;
	case "saturday" :
		console.log("The color of the day is indigo.");
		break;
	case "sunday" :
		console.log("The color of the day is violet.");
		break;
	default :
		console.log("Please enter a valid day.")
		break;
}

