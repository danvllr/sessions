// console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper

function numberLooper() {
	let number = Number(prompt("Please enter a number."));

	console.log("The number you provided is " + number);

	for (let i = number; i > 0; i--) {

		if  (i % 10 === 0) {
			console.log("The number is divisible by 10. Skipping the number.");
			continue;
		}

		if (i % 5 === 0) {
			console.log(i);
		}

		if (i <= 50) {
			console.log("The current value is 50. Terminating loop.");
			break;
		}
	}
}

numberLooper();


//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for (let i = 0; i < string.length; i++) {

	if (string[i].toLowerCase() == "a" ||
		string[i].toLowerCase() == "e" ||
		string[i].toLowerCase() == "i" ||
		string[i].toLowerCase() == "o" ||
		string[i].toLowerCase() == "u") {
		continue;
	} else {
		filteredString += string[i];
	}
}

console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}