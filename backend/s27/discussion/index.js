// Array Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push();

// Add element at the end of an array

console.log("Current Array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");

console.log("Array after push method:");
console.log(fruitsLength);
console.log(fruits);

// Push multiple elements

fruits.push("Avocado", "Guava");
console.log("Array after push method:");
console.log(fruits);

function addFruit (fruit) {
	// push parameter
	fruits.push(fruit);
	console.log(fruits);
}

addFruit("Pineapple");

// pop method : pop();

// Remove an element at the end of an array

fruits.pop();
fruits.pop();
console.log("Array after pop method:");
console.log(fruits);

function removeFruit () {
	fruits.pop();
	console.log(fruits);
}

removeFruit();

// unshift method : unshift();

// Add element in the beginning of an array

fruits.unshift("Lime", "Banana");
console.log("Array after unshift method:");
console.log(fruits);

function unshiftFruit (fruit) {
	fruits.unshift(fruit);
	console.log(fruits);
}

unshiftFruit("Calamansi");

// shift method : shift();

// Remove an element in the beginning of an array

fruits.shift();
console.log("Array after shift method:");
console.log(fruits);

function shiftFruit () {
	fruits.shift();
	console.log(fruits);
}

shiftFruit();

// splice method: splice();

// Simultaneously removes elements from a spliced index number and adds elements

// Syntax: array.splice(startIndex, deleteCount, elemetsToBeAdded);

fruits.splice(1, 2, "Avocado");
console.log("Array after splice method:");
console.log(fruits);

function spliceFruit (index, deleteCount, fruit) {
	fruits.splice(index, deleteCount, fruit);
	console.log(fruits);
}

spliceFruit(1, 1, "Cherry");

spliceFruit(2, 1, "Durian");

// sort method : sort();

// Arranges the elements in alphanumeric order

fruits.sort();
console.log("Array after sort method:");
console.log(fruits);

// reverse method : reverse ();

// Arranges the elements in alphanumeric order (reveresed order)

fruits.reverse();
console.log("Array after reverse method:");
console.log(fruits);
