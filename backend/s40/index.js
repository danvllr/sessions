const express = require("express");

const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));
// Accepts all data type

// [SECTION] Routes

// GET method
app.get("/", (req, res) => {
	res.send("Hello World!");
})

app.get("/hello", (req,res) => {
	res.send("Hello from /hello end point");
})

// route expects to receive a Post methof at the URI "/hello"

app.post("/hello", (req, res) => {
	res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
})

// POST route to register user

let users = [];


// Challenge
app.post("/signup", (request, response) => {	
	console.log(request.body);
	if(request.body.username !== "" && request.body.password !== ""){
		let doesExist = false;
		for(let index = 0 ; index < users.length ; index++){
			if(request.body.username === users[index].username){
				doesExist = true;
				break;
			}
		}
		if(doesExist){
			response.send("User already exists!");
		}else{
			users.push(request.body)	
			response.send(`Hi ${request.body.username}`);
		}


	}else{
		response.send("Username or Password is missing.");
	}
});

app.put("/change-password", (request, response) => {
	let message = "No user is currently registered.";
	if(users.length > 0){
		message = "User does not exist.";
		for(let index = 0; index < users.length; index++){
			if(users[index].username === request.body.username){
				users[index].password = request.body.password;
				message = `User ${request.body.username}'s password has been updated.`
				break;
			}
		}
	}
	response.send(message);			
});



if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`));
}