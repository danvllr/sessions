// CRUD Operations (MongoDB)

// Crud Operations are the heart of any backend app.
// Mastering Crud Operations is essential for any developers.

/*

C - Create/Insert
R - Retrieve/Read
U - Update
D - Delete

*/

// [SECTION] Creating documents

/*

SYNTAX: 

db.users.insertOne ({object})

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "Python"],
	department: "none"
});

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});



db.users.insertOne({
	firstName: "Danna",
	lastName: "Villar",
	age: 26,
	contact: {
		phone: "09186442447",
		email: "villardannamei@gmail.com"
	},
	courses: ["CSS", "JS", "Python"],
	department: "none"
});


// Insert Many
/*

SYNTAX: 

db.users.insertMany ([{object}])

*/


db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["PHP", "React", "Python"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);


// [SECTION] Read/Retrieve

/*

Syntax:

db.users.findOne();
db.users.findOne({field: value});


*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});

/*

Syntax:

db.users.find();
db.users.find({field: value});


*/

db.users.find({department: "none"});


// multiple criteria

db.users.find({department: "none", age: 82});


// [SECTION] Updating a data

/*

Syntax:

db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
{firstName: "Test"},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "123456789",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
}
);

// find the data
db.users.findOne({firstName: "Bill"});
db.users.findOne({firstName: "Test"});

db.users.find({"contact.email": "billgates@gmail.com"});

// Update many collection


/*

Syntax:

db.collectionName.updateMany({criteria}, {$set: {field: value}});

*/

db.users.updateMany(
{department: "none"},
{
	$set: {
		department: "HR"
	}
}
);

// [SECTION] Deleting a data

db.users.insert({
	firstName:"Test"
});

// Delet single docoment

/*

Syntax:

db.users.deleteOne({criteria});

*/

db.users.deleteOne({
	firstName: "Test"
});

// Delete Many

// Insert another bill

db.users.insert({
	firstName:"Bill"
});

/*

Syntax:

db.users.deleteMany({criteria});

*/

db.users.deleteMany({firstName: "Bill"});