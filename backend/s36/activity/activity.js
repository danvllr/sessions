async function fruitsOnSale(db) {
    return await(
        db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: null, fruitsOnSale: {$sum: 1}}},
            {$project: {_id: 0, fruitsOnSale: 1}}
        ]);
        );
};

async function fruitsInStock(db) {
    return await(
        db.fruits.aggregate([
            {$match: {stock: {$gt: 20}}},
            {$group: {_id: null, enoughStock: {$sum: 1}}},
            {$project: {_id: 0, enoughStock: 1}}
        ]);
        );
};

async function fruitsAvePrice(db) {
    return await(
        db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
            {$project: {_id: 1, avg_price: 1}}
        ]);
        );
};

async function fruitsHighPrice(db) {
    return await(
        db.fruits.aggregate([
            {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
            {$project: {_id: 1, max_price: 1}}
        ]);
        );
};

async function fruitsLowPrice(db) {
    return await(
        db.fruits.aggregate([
            {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
            {$project: {_id: 1, min_price: 1}}
        ]);
        );
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};