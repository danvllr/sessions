const express = require('express');
const auth = require("../auth.js")
const {verify, verifyAdmin} = auth;
const router = express.Router();
const courseController = require('../controllers/course');


// Create a course POST
router.post('/', verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get("/all", courseController.getAllCourses);


// Get all "active" course

router.get('/', courseController.getAllActive);

// Get 1 specific course using ID

router.get("/:courseId", courseController.getCourse);

// Updating a Course (Admin Only)

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archiving a course (Admin Only)

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// Activating a course (Admin Only)

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);



module.exports = router;