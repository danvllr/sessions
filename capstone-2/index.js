// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/user'); 
const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');

// [ SECTION ] Environment Setup
const port = 4000;

// [ SECTION ] Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors()); 

// [ SECTION ] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.3jj5lmo.mongodb.net/Capstone2_API?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};

module.exports = app;
