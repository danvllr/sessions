const Order = require('../models/Order');
const Product = require('../models/Product'); 
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Non-admin User Checkout
module.exports.createOrder = async (req, res) => {
	try {
	    const { userId, products } = req.body;

	    let totalAmount = 0;

	    const orderDetails = [];

	    for (const productItem of products) {
			const { productId, quantity } = productItem;

			const product = await Product.findById(productId);

			if (!product || !product.isActive) {
				return res.status(400).json({ error: 'One or more products in the order are not available' });
			}

			const productTotal = product.price * quantity;
			totalAmount += productTotal;

			orderDetails.push({
				productId,
				quantity,
				productTotal,
			});
		}

	    const order = new Order({
			userId,
			products: orderDetails,
			totalAmount,
		});

		await order.save();

		res.status(201).json({ message: 'Order created successfully', order });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: 'Order creation failed' });
	}
};

// Retrieve Authenticated User's Orders
module.exports.getUserOrders = async (req, res) => {
	const userId = req.user.id; 

	try {
		const userOrders = await Order.find({ userId });

		res.status(200).json({ orders: userOrders });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: 'Internal server error' });
	}
};

// Retrieve All Orders (Admin Only)
module.exports.getAllOrders = async (req, res) => {
	try {
		const orders = await Order.find().populate('userId', 'firstName lastName email'); 
		res.status(200).json({ orders });
	} catch (error) {
		console.error(error);
		res.status(500).json({ error: 'Internal server error' });
	}
};