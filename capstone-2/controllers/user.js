const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User Registraion
module.exports.registerUser = (reqBody) => {
	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
	.catch(err => err);
};

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result === null){
			return false; 
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		};
	});
};

// Retrieve User Details
module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id)
    .select('-password -isAdmin') // Exclude password and isAdmin fields
    .then(user => {
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
      return res.json(user);
    })
    .catch(error => {
      console.error(error);
      return res.status(500).json({ error: 'Internal server error' });
    });
};

// Set User as Admin (Admin Only)
module.exports.setAdmin = async (req, res) => {
	const userId = req.params.userId;

	try {
		const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ isAdmin: true },
			{ new: true } 
	    );

		if (!updatedUser) {
			return res.status(404).json({ error: 'User not found' });
		}

		res.status(200).json({ message: 'User role set to admin', user: updatedUser });
		} catch (error) {
			console.error(error);
			res.status(500).json({ error: 'Internal server error' });
	}
};