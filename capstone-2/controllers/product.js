const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Create Product (Admin Only)
module.exports.createProduct = (req, res) => {
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	newProduct.save().then(() => {
      return res.send(true);
    })
    .catch((error) => {
    	console.error(error);
    	return res.send(false);
    });
};

// Retrieve All Products
module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		if(result.length === 0) {
			return res.send(result);
		} else {
			return res.send(result);
		}
	});
};

// Retrieve All Active Products
module.exports.getAllActive = (req, res) => {
	return Product.find({isActive : true}).then(result => {
		if(result.length === 0) {
			return res.send("There is currently no active products.")
		} else {
			return res.send(result);
		}
	});
};


// Retrieve Single Product
module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {
		if(result === 0) {
			res.send("Cannot find product with the provided ID.")
		} else {
			if(result.isActive === false) {
				return res.send("The product you are trying to access is not available.")
			} else {
				return res.send(result);
			}
		};
	})
	.catch(error => res.send("Please enter a correct product ID."))
};

// Update Product Information (Admin Only)
module.exports.updateProduct = (req, res) => {
	let updatedProduct = {
		name: req.body.name,
    	description: req.body.description,
    	price: req.body.price
	};

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
		if(error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	});
};

// Archive Product (Admin Only)

module.exports.archiveProduct = (req, res) => {
	let updatedProduct = { isActive: false };
	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send("Please enter a correct product ID."))
};

// Activate Product (Admin Only)
module.exports.activateProduct = (req, res) => {
	let updatedProduct = { isActive: true };
	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((course, error) => {
		if(error) {
			return res.send(false)
		} else {
			return res.send(true);
		}
	})
	.catch(error => res.send("Please enter a correct Product ID."))
};


