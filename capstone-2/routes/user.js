const express = require('express');
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth;
const router = express.Router();
const userController = require('../controllers/user.js');

// User Registraion
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Retrieve User Details
router.get('/details', auth.verify, userController.getProfile);

// Set User as Admin (Admin Only)
router.put('/set-admin/:userId', userController.setAdmin);


module.exports = router;
