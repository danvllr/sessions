const express = require('express');
const auth = require('../auth.js')
const router = express.Router();
const orderController = require('../controllers/order.js');

// Non-admin User Checkout
router.post('/create', auth.verify, orderController.createOrder);

// Retrieve Authenticated User's Orders
router.get('/my-orders', auth.verify, orderController.getUserOrders);

// Retrieve All Orders (Admin Only)
router.get('/all-orders', auth.verify, auth.verifyAdmin, orderController.getAllOrders);

module.exports = router;
