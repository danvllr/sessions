const express = require('express');
const auth = require('../auth.js')
const {verify, verifyAdmin} = auth;
const router = express.Router();
const productController = require('../controllers/product.js');

// Create Product (Admin Only)
router.post("/create",verify, verifyAdmin, productController.createProduct);

// Retrieve All Products
router.get("/all", productController.getAllProducts);

// Retrieve All Active Products
router.get("/", productController.getAllActive);

// Retrieve Single Product
router.get("/:productId", productController.getProduct);

// Update Product Information (Admin Only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Archive Product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Activate Product (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


module.exports = router;
