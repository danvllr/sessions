//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const orderSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
    	required : [true, "User ID is required"]
	},
	products : [
		{
			productId : {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: [true, "Product ID is required"]
			},
			quantity : {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}

	],
	totalAmount : {
		type: Number,
		required: true
	},
	purchasedOn : { 
		type: Date,
		default: new Date()
	}
});

//[SECTION] Model
module.exports = mongoose.model("Order", orderSchema);
 